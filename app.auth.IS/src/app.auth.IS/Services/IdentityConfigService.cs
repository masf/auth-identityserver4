﻿using app.auth.IS.Model.ConfigModel;
using Microsoft.Extensions.Configuration;


namespace app.auth.IS.Services
{
    public class IdentityConfigService
    {
        private readonly IdentityServerConfig _identityServiceConfig;
        public IdentityConfigService(IConfigurationRoot configuration)
        {
            _identityServiceConfig = new IdentityServerConfig();
            configuration.GetSection("IdentityServerConfig").Bind(_identityServiceConfig);
        }

        public IdentityServerConfig IdentityServiceConfig
        {
            get
            {
                return _identityServiceConfig;
            }
        }
    }
}
