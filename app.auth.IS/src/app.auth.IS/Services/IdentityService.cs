﻿using app.auth.IS.Model.ConfigModel;
using app.auth.IS.Model.ConfigModel;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app.auth.IS.Services
{
    public class IdentityService
    {
        GoogleIdentityConfig _googleIdentityConfig;
        FacebookIdentityConfig _facebookIdentityConfig;
        TwitterIdentityConfig _twitterIdentityConfig;

        public IdentityService(IConfigurationRoot configuration)
        {
            _googleIdentityConfig = new GoogleIdentityConfig();
            configuration.GetSection("GoogleIdentityConfig").Bind(_googleIdentityConfig);

            _facebookIdentityConfig = new FacebookIdentityConfig();
            configuration.GetSection("FacebookIdentityConfig").Bind(_facebookIdentityConfig);

            _twitterIdentityConfig = new TwitterIdentityConfig();
            configuration.GetSection("TwitterIdentityConfig").Bind(_twitterIdentityConfig);
        }

        public GoogleIdentityConfig GoogleIdentityConfig
        {
            get
            {
                if (_googleIdentityConfig == null) throw new Exception("O google identity não está configurado ");
                return _googleIdentityConfig;
            }
        }

        public FacebookIdentityConfig FacebookIdentityConfig
        {
            get
            {
                if (_facebookIdentityConfig == null) throw new Exception("O facebook identity não está configurado ");
                return _facebookIdentityConfig;
            }
        }

        public TwitterIdentityConfig TwitterIdentityConfig
        {
            get
            {
                if (_facebookIdentityConfig == null) throw new Exception("O twitter identity não está configurado ");
                return _twitterIdentityConfig;
            }
        }
    }
}
