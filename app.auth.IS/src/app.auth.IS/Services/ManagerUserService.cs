﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Security.Claims;
using app.auth.IS.Model.SystemModel;

namespace app.auth.IS.Services
{
    public class ManagerUserService
    {
        private UserManager<ApplicationUser> _userManager;
        public ManagerUserService(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async void CreateSeedAdmin()
        {
            ApplicationUser user = await _userManager.FindByEmailAsync("TestUser@test.com");
            if (user != null) await _userManager.DeleteAsync(user);

            user = new ApplicationUser
            {
                UserName = "admin",
                Email = "TestUser@test.com",
            };

            var result = await _userManager.CreateAsync(user, "Admin@123");
            if (!result.Succeeded)
            {
                await _userManager.AddClaimAsync(user, new System.Security.Claims.Claim("name", "sdkvjd vd"));
                await _userManager.AddClaimAsync(user, new System.Security.Claims.Claim("office", "sdkvjd vd"));
                await _userManager.AddClaimAsync(user, new System.Security.Claims.Claim("role", "sdkvjd vd"));
            }

        }

        public async Task<ApplicationUser> LoginAsync(string email, string password)
        {
            ApplicationUser user= await _userManager.FindByEmailAsync(email);
            if (user == null) throw new Exception("Usuario ou senha incorreto");

            if(!await _userManager.CheckPasswordAsync(user, password))
            {
                throw new Exception("Usuario ou senha incorreto");
            }
            return user;
        }

        public async Task<ApplicationUser> FindByExternalProvider(string provider, string emailId)
        {
            return await _userManager.FindByLoginAsync(provider,  emailId);
        }

        public async Task<ApplicationUser> CreateAsync(string provider, string emailId, string userId, List<Claim> claims)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = emailId,
                Email = emailId,
            };

            ApplicationUser userApplication = await _userManager.FindByEmailAsync(emailId);
            IdentityResult result;
            if (userApplication == null)
            {
                result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    foreach (Claim claim in claims)
                    {
                        await _userManager.AddClaimAsync(user, new System.Security.Claims.Claim(claim.Type, claim.Value));
                    }
                }
            }           

            result = await _userManager.AddLoginAsync(user, new UserLoginInfo(provider, userId, provider));

            user = await _userManager.FindByLoginAsync(provider, emailId);
            return user;
        }

        public void AddClaimAsync(ApplicationUser user, List<Claim> listClaims)
        {
            foreach (Claim claim in listClaims)
            {
                _userManager.AddClaimAsync(user, claim);
            }
            
        }
    }
}
