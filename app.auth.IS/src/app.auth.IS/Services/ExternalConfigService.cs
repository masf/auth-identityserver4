﻿using app.auth.IS.Model.ConfigModel;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;

namespace app.auth.IS.Services
{
    public class ExternalConfigService
    {
        ExternalConfig _externalConfig;

        public ExternalConfigService(IConfigurationRoot configuration)
        {
            _externalConfig = new ExternalConfig();
            configuration.GetSection("ExternalConfig").Bind(_externalConfig);
        }

        public ExternalConfig ExternalTokenConfig
        {
            get
            {
                return _externalConfig;
            }
        }

        public ExternalTokenConfig ExternalGoogleConfig
        {
            get
            {
                ExternalTokenConfig externalConfig = this.ExternalTokenConfig.ExternalTokenList.FirstOrDefault(p => p.Type.Equals("Google"));
                if (externalConfig == null)
                    throw new Exception("A configuração externa do google não existe");

                return externalConfig;
            }
        }
    }
}
