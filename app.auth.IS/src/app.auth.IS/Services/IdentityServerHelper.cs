﻿using app.auth.IS.Model.ConfigModel;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace app.auth.IS.Services
{
    public class IdentityServerHelper
    {
        ExternalConfigService _externalConfigService;

        public IdentityServerHelper(ExternalConfigService externalConfigService)
        {
            _externalConfigService = externalConfigService;
        }

        private ExternalTokenConfig GetExternalTokenConfig(string type)
        {
            return _externalConfigService.ExternalTokenConfig
                .ExternalTokenList.FirstOrDefault(p => p.Type.ToUpper().Equals(type.ToUpper()));
        }

        public async Task<JObject> RequestClientCredentialsAsync(string typeExternalToken, string userId, string email, ClaimsPrincipal claimsPrincipal, params Claim[] claims)
        {
            ExternalTokenConfig externalTokenConfig =  GetExternalTokenConfig(typeExternalToken);
            var disco = await DiscoveryClient.GetAsync("http://localhost:5000");

            var tokenClient = new TokenClient(disco.TokenEndpoint, externalTokenConfig.IdentityServerClientId, externalTokenConfig.IdentityServerClientSecret);

            Dictionary<string, string> payload = new Dictionary<string, string>();
            payload.Add("userId", userId);
            payload.Add("email", email);
            payload.Add("scope", externalTokenConfig.Scope);
            payload.Add("provider", typeExternalToken);

            Dictionary<string, string> claimsDictionary = new Dictionary<string, string>();
            foreach (Claim claim in claims)
            {
                claimsDictionary.Add(claim.Type, claim.Value);
            }
            payload.Add("claims", JObject.FromObject(claimsDictionary).ToString());

            var tokenResponse = await tokenClient.RequestCustomGrantAsync("cse_social", externalTokenConfig.Scope, payload);
            return tokenResponse.Json;
        }

    }
}
