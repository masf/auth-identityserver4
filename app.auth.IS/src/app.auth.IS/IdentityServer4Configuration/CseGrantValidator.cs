﻿using app.auth.IS.Model.SystemModel;
using app.auth.IS.Services;
using IdentityServer4.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace app.auth.IS.IdentityServer4Configuration
{
    public class CseGrantValidator : IExtensionGrantValidator
    {
        private readonly ITokenValidator _validator;
        private readonly ManagerUserService _managerUserService;
        public CseGrantValidator(ITokenValidator validator, ManagerUserService managerUserService)
        {
            _validator = validator;
            _managerUserService = managerUserService;
        }

        public string GrantType => "cse";

        public async Task ValidateAsync(ExtensionGrantValidationContext context)
        {
            var username = context.Request.Raw.Get("email");
            var password = context.Request.Raw.Get("password");
            try
            {
                ApplicationUser user = await _managerUserService.LoginAsync(username, password);

                IEnumerable<Claim> claims = user.Claims.Select(p => new Claim(p.ClaimType, p.ClaimValue));
                context.Result = new GrantValidationResult(user.Id, "custom", claims: claims);
                context.Result.CustomResponse = new Dictionary<string, object>();
                context.Result.CustomResponse.Add("username", user.UserName);
                context.Result.CustomResponse.Add("claims", user.Claims.Select(p => new { Key = p.ClaimType, Value = p.ClaimValue }).ToArray());
            }
            catch (Exception ex)
            {
                context.Result = new GrantValidationResult(username, IdentityModel.OidcConstants.AuthorizeErrors.AccessDenied);
                context.Result.IsError = true;
                context.Result.ErrorDescription = IdentityModel.OidcConstants.AuthorizeErrors.AccessDenied;
                context.Result.Error = ex.Message;
            }            

            return;
        }
    }
}

