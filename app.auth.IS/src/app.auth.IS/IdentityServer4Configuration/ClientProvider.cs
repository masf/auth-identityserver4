﻿using IdentityServer4.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4;
using app.auth.IS.Model.ConfigModel;
using app.auth.IS.Services;

namespace app.auth.IS.IdentityServer4Configuration
{
    public class ClientProvider : IClientStore
    {
        IdentityConfigService _identityServerConfig;
        public ClientProvider(IdentityConfigService identityServerConfig)
        {
            _identityServerConfig = identityServerConfig;
        }

        public IEnumerable<Client> AllClients
        {
            get
            {
                List<Client> clientList = new List<Client>();
                foreach (ClientConfig clientConfig in _identityServerConfig.IdentityServiceConfig.ClientList)
                {
                    clientList.Add(
                    new Client
                    {
                        ClientId = clientConfig.ClientId,
                        ClientName = clientConfig.ClientDescription,
                        AccessTokenLifetime = clientConfig.TimeLife,
                        AllowedGrantTypes = clientConfig.GrantTypes,
                        RequireClientSecret = clientConfig.RequireClientSecret,
                        ClientSecrets = new[] { new Secret((clientConfig.ClientSecret ?? "").Sha256()) },
                        AllowAccessTokensViaBrowser = clientConfig.AllowAccessInBrowser,
                        AllowedScopes = clientConfig.Scopes,
                        RedirectUris = clientConfig.RedirectUris,
                        PostLogoutRedirectUris = clientConfig.PostLogoutRedirectUris,
                        AllowedCorsOrigins = clientConfig.Authority,
                        AllowOfflineAccess = clientConfig.AllowOfflineAccess,
                        //ClientUri = "http://localhost:5000",

                    });
                }
                return clientList;

            }
        }

        public Task<Client> FindClientByIdAsync(string clientId)
        {
            return Task.FromResult(AllClients.FirstOrDefault(c => c.ClientId == clientId));
        }
    }
}
