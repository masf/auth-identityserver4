﻿using app.auth.IS.Model.SystemModel;
using app.auth.IS.Services;
using IdentityModel;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace app.auth.IS.IdentityServer4Configuration
{
    public class CseSocialGrantValidator : IExtensionGrantValidator
    {
        private readonly ITokenValidator _validator;
        private readonly ICustomTokenRequestValidator _customTokenRequestValidator;
        private readonly ManagerUserService _managerUserService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CseSocialGrantValidator(ITokenValidator validator, ManagerUserService managerUserService, IHttpContextAccessor httpContextAccessor)
        {
            _validator = validator;
            _managerUserService = managerUserService;
            _httpContextAccessor = httpContextAccessor;
        }

        public string GrantType => "cse_social";

        public async Task ValidateAsync(ExtensionGrantValidationContext context)
        {
            string userId = context.Request.Raw.Get("userId");
            string userEmail = context.Request.Raw.Get("email");
            string provider = context.Request.Raw.Get("provider");
            string claimsJson = context.Request.Raw.Get("claims");
            string scope = context.Request.Raw.Get("scope");


            try
            {
                ApplicationUser user = await _managerUserService.FindByExternalProvider(provider, userId);
                List<Claim> listClaims = new List<Claim>();
                if (user == null)
                {
                    Dictionary<string, string> claimsDicitionary = JObject.Parse(claimsJson).ToObject<Dictionary<string, string>>();
                    
                    listClaims.AddRange(claimsDicitionary.Select(p => new Claim(p.Key, p.Value)));

                    user = await _managerUserService.CreateAsync(provider, userEmail, userId, listClaims);
                }

                listClaims.AddRange(user.Claims.Select(p => new Claim(p.ClaimType, p.ClaimValue)));

                _managerUserService.AddClaimAsync(user, listClaims);

                Dictionary<string,object> customResponse = new Dictionary<string, object>();
                customResponse.Add("username", user.UserName);
                customResponse.Add("claims", listClaims.Select(p => new { Key = p.Type, Value = p.Value }).ToArray());

                context.Result = new GrantValidationResult(user.Id, "custom", claims: listClaims, customResponse: customResponse);
            }
            catch (Exception ex)
            {
                context.Result = new GrantValidationResult(userId, OidcConstants.AuthorizeErrors.AccessDenied);
                context.Result.IsError = true;
                context.Result.ErrorDescription = OidcConstants.AuthorizeErrors.AccessDenied;
                context.Result.Error = ex.Message;
            }
            return;
        }
    }
}



