﻿using System.Collections.Generic;
using IdentityServer4.Models;
using app.auth.IS.Model.ConfigModel;

namespace app.auth.IS.IdentityServer4Configuration
{
    internal class ApiResourceProvider
    {
        public static IEnumerable<ApiResource> GetAllResources(IdentityServerConfig identityServerConfig)
        {
            List<ApiResource> apiResouces = new List<ApiResource>();
            foreach (ClientConfig clientConfig in identityServerConfig.ClientList)
            {
                foreach (string scope in clientConfig.Scopes)
                {
                    apiResouces.Add(new ApiResource(scope, $"{clientConfig.ClientDescription} {scope} Resource", clientConfig.Claims));
                }
            }
            return apiResouces;
        }
    }
}