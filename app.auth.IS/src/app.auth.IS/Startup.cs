﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using IdentityServer4.Stores;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using app.auth.IS.Data;
using app.auth.IS.Services;
using app.auth.IS.Model.SystemModel;
using app.auth.IS.IdentityServer4Configuration;

namespace app.auth.IS
{
    public class Startup
    {

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            Configuration = builder.Build();
        }


        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddSingleton(this.Configuration);
            services.AddSingleton<IClientStore, ClientProvider>();
            services.AddSingleton<ClientProvider>();
            services.AddScoped<ManagerUserService>();
            services.AddScoped<IdentityService>();
            services.AddSingleton<IdentityConfigService>();
            services.AddScoped<ExternalConfigService>();
            services.AddScoped<IdentityServerHelper>();

            IServiceProvider serviceProvider = services.BuildServiceProvider();
            IdentityConfigService identityConfigService = serviceProvider.GetService<IdentityConfigService>();

            services.AddIdentityServerConfig(identityConfigService);
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, RoleManager<IdentityRole> roleManager, ManagerUserService managerUserService, IdentityService identityService)
        {
            loggerFactory.AddConsole();

            ApplicationRoles.InitializeRoles(roleManager).Wait();

            app.UseIdentity();
            app.UseIdentityServer();
            app.UseGoogleAuth(identityService.GoogleIdentityConfig);
            app.UseFaceboobkAuth(identityService.FacebookIdentityConfig);
            app.UseTwitterAuth(identityService.TwitterIdentityConfig);
            app.UseMvcWithDefaultRoute();

            managerUserService.CreateSeedAdmin();
        }
    }
}
