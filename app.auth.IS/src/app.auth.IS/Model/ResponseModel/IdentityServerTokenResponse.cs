﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app.auth.IS.Model.ResponseModel
{
    public class IdentityServerTokenResponse
    {

        public IdentityServerTokenResponse(string urlAccess)
        {
            this.UrlAccess = urlAccess;
        }

        public IdentityServerTokenResponse(string urlAccess, Exception exception): this(urlAccess)
        {
            this.IsError = true;
            this.ErrorDescription = exception.Message;
        }

        public bool IsError { get; }
        public string ErrorDescription { get; }
        public string UrlAccess { get; }
    }
}
