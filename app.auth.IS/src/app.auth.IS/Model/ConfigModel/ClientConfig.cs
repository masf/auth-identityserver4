﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app.auth.IS.Model.ConfigModel
{
    public class ClientConfig
    {
        public string ClientId { get; set; }
        public string ClientDescription { get; set; }
        public string ClientSecret { get; set; }
        public bool RequireClientSecret { get; set; }
        public bool AllowAccessInBrowser { get; set; }
        public int TimeLife { get; set; }
        public string[] Scopes { get; set; }
        public string[] Claims { get; set; }
        public string[] GrantTypes { get; set; }
        public string[] RedirectUris { get; set; }
        public string[] PostLogoutRedirectUris { get; set; }
        public string[] Authority { get; set; }
        public bool AllowOfflineAccess { get; set; }
    }
}
