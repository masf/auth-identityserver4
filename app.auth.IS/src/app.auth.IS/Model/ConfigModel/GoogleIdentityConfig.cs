﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app.auth.IS.Model.ConfigModel
{
    public class GoogleIdentityConfig
    {
        public string AuthenticationScheme { get; set; }
        public string DisplayName { get; set; }
        public string SignInScheme { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public bool RequireHttpsMetadata { get; set; }
        public string[] ScopeExternal { get;  set; }
    }
}
