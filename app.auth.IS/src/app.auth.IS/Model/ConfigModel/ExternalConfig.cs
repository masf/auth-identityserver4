﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app.auth.IS.Model.ConfigModel
{
    public class ExternalTokenConfig
    {
        public string Type { get; set; }
        public string IdentityServerClientId { get; set; }
        public string IdentityServerClientSecret { get; set; }
        public string Scope { get; set; }
    }
}
