﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app.auth.IS.Model.ConfigModel
{
    public class IdentityServerConfig
    {
        public ClientConfig[] ClientList { get; set; }
    }
}
