﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app.auth.IS.Model.ConfigModel
{
    public class TwitterIdentityConfig
    {
        public string AuthenticationScheme { get; set; }
        public string DisplayName { get; set; }
        public string SignInScheme { get; set; }
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public bool RequireHttpsMetadata { get; set; }
    }
}
