﻿using app.auth.IS.Model.ConfigModel;
using app.auth.IS.Model.ConfigModel;
using IdentityServer4;
using Microsoft.IdentityModel.Tokens;

namespace Microsoft.AspNetCore.Builder
{
    public static class FacebookConfigurationApplicationExtensions
    {
        public static IApplicationBuilder UseFaceboobkAuth(this IApplicationBuilder app, FacebookIdentityConfig facebookIdentityConfig)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = facebookIdentityConfig.SignInScheme,

                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });

          FacebookOptions option = new FacebookOptions()
            {
                AuthenticationScheme = facebookIdentityConfig.AuthenticationScheme,
                DisplayName = facebookIdentityConfig.DisplayName,
                SignInScheme = facebookIdentityConfig.SignInScheme,
                AppId = facebookIdentityConfig.AppId,
                AppSecret = facebookIdentityConfig.AppSecret
            };

            foreach (string scopeExternal in facebookIdentityConfig.ScopeExternal)
            {
                option.Scope.Add(scopeExternal);
            }
            app.UseFacebookAuthentication(option);            
            return app;
        }
    }
}
