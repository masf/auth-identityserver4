﻿using app.auth.IS.Model.Config;
using app.auth.IS.Model.ConfigModel;
using IdentityServer4;
using Microsoft.IdentityModel.Tokens;

namespace Microsoft.AspNetCore.Builder
{
    public static class TwitterConfigurationApplicationExtensions
    {
        public static IApplicationBuilder UseTwitterAuth(this IApplicationBuilder app, TwitterIdentityConfig twitterIdentityConfig)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = twitterIdentityConfig.SignInScheme,

                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });

          TwitterOptions option = new TwitterOptions()
            {
                AuthenticationScheme = twitterIdentityConfig.AuthenticationScheme,
                DisplayName = twitterIdentityConfig.DisplayName,
                SignInScheme = twitterIdentityConfig.SignInScheme,
                ConsumerKey = twitterIdentityConfig.ConsumerKey,
                ConsumerSecret = twitterIdentityConfig.ConsumerSecret
          };

            app.UseTwitterAuthentication(option);            

            return app;
        }
    }
}
