﻿using app.auth.IS.Model.ConfigModel;

namespace Microsoft.AspNetCore.Builder
{
    public static class GoogleConfigurationApplicationExtensions
    {
        public static IApplicationBuilder UseGoogleAuth(this IApplicationBuilder app, GoogleIdentityConfig googleIdentityConfig)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = googleIdentityConfig.SignInScheme,

                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });

            var optionGoogle = new GoogleOptions
            {
                AuthenticationScheme = googleIdentityConfig.AuthenticationScheme,
                DisplayName = googleIdentityConfig.DisplayName,
                SignInScheme = googleIdentityConfig.SignInScheme,
                ClientId = googleIdentityConfig.ClientId,
                ClientSecret = googleIdentityConfig.ClientSecret,
                SaveTokens =true
            };

            if (googleIdentityConfig.ScopeExternal != null)
                foreach (string scopeExternal in googleIdentityConfig.ScopeExternal)
                {
                    optionGoogle.Scope.Add(scopeExternal);
                }

            app.UseGoogleAuthentication(optionGoogle);

            return app;
        }
    }
}
