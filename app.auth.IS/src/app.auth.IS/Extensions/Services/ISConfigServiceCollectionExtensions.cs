﻿using app.auth.IS.IdentityServer4Configuration;
using app.auth.IS.Model.SystemModel;
using app.auth.IS.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ISConfigServiceCollectionExtensions
    {
        public static IServiceCollection AddIdentityServerConfig(this IServiceCollection services, IdentityConfigService identityConfigService)
        {
            IIdentityServerBuilder iIdentityServerBuilder = services.AddIdentityServer();
            
            iIdentityServerBuilder.AddTemporarySigningCredential();
            //iIdentityServerBuilder.AddSigningCredential(new X509Certificate2(Path.Combine(".", "certs", "IdentityServer4Auth.pfx")))
            iIdentityServerBuilder.AddInMemoryApiResources(ApiResourceProvider.GetAllResources(identityConfigService.IdentityServiceConfig));
            iIdentityServerBuilder.AddAspNetIdentity<ApplicationUser>();
            iIdentityServerBuilder.AddExtensionGrantValidator<CseGrantValidator>();
            iIdentityServerBuilder.AddExtensionGrantValidator<CseSocialGrantValidator>();
            return services;
        }
    }
}
