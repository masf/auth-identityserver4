﻿using IdentityModel;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using app.auth.IS.Services;
using IdentityServer4;
using app.auth.IS.Model;
using Microsoft.AspNetCore.Authorization;
using System.Security.Principal;
using Newtonsoft.Json.Linq;

namespace app.auth.IS.Controllers
{
    [Route("AccountGoogle")]
    [AllowAnonymous]
    public class AccountGoogleController : Controller
    {
        private readonly ManagerUserService _managerUserService;
        private readonly IdentityServerHelper _identityServerHelper;

        public AccountGoogleController(ManagerUserService managerUserService, IdentityServerHelper identityServerHelper)
        {
            _managerUserService = managerUserService;
            _identityServerHelper = identityServerHelper;
        }

        [HttpGet]
        public IActionResult Get(string accessUrl, string unaccessUrl, string returnUrl)
        {
            if (string.IsNullOrEmpty(accessUrl)) throw new Exception("A url de access é obrigatoria para executar esse comando");

            returnUrl = Url.Action("GoogleLoginCallback", new
            {
                accessUrl = accessUrl,
                unaccessUrl = unaccessUrl,
                returnUrl = returnUrl,
            });

            var props = new AuthenticationProperties
            {
                RedirectUri = returnUrl,
                Items = { { "scheme", "Google" } }
            };
            return new ChallengeResult("Google", props);
        }

        [HttpGet]
        [Route("GoogleLoginCallback")]
        public async Task<IActionResult> GoogleLoginCallback(string accessUrl, string unaccessUrl, string returnUrl)
        {
            var info = await HttpContext.Authentication.GetAuthenticateInfoAsync(IdentityServerConstants.ExternalCookieAuthenticationScheme);
            var tempUser = info?.Principal;
            if (tempUser == null)
            {
                throw new Exception("External authentication error");
            }

            var claims = tempUser.Claims.ToList();
            var userIdClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
            if (userIdClaim == null)
            {
                throw new Exception("Unknown userid");
            }
            var userEmailClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.Email);
            claims.Remove(userIdClaim);
            var provider = info.Properties.Items["scheme"];
            var userId = userIdClaim.Value;
            var userEmail = userEmailClaim.Value;

            try
            {
                JObject result = await _identityServerHelper.RequestClientCredentialsAsync(provider, userId, userEmail, HttpContext.User, claims.ToArray());
                return Redirect(returnUrl + $"#access_token={result.GetValue("access_token")}&username={result.GetValue("username")}");
            }
            catch (Exception ex)
            {
                return Redirect(unaccessUrl);
            }


            
        }
    }
}