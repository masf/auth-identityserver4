﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.auth.IS.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public async void Access()
        {
            await HttpContext.Authentication.GetTokenAsync("access_token");
        }
    }
}
