import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SiginExternalComponent }   from '../Component/sigin-external.component';
import { LoginComponent }   from '../Component/login.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login',  component: LoginComponent },
  { path: '', redirectTo: '/sigin-external',pathMatch: 'full' },
  { path: 'sigin-external', component: SiginExternalComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}