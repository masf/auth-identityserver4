import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {APP_BASE_HREF} from '@angular/common';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SiginExternalComponent } from './Component/sigin-external.component';
import { LoginComponent } from './Component/login.component';

import { SiginExternalService } from './Service/sigin-external.service';

import { AppRoutingModule }     from './module/app-router.module';


@NgModule({
  imports: [BrowserModule, HttpModule, JsonpModule, FormsModule, AppRoutingModule],
   declarations: [
    AppComponent,
    SiginExternalComponent,
    LoginComponent
  ],
  providers: [
    [SiginExternalService],
    [{provide: APP_BASE_HREF, useValue : '/' }]
  ],
  bootstrap: [AppComponent]

})
export class AppModule { }
