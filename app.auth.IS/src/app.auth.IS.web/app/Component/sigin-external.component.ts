import 'rxjs/add/operator/map'
import 'rxjs/Rx';

import { Component, OnInit } from '@angular/core';
import { SiginExternalService } from '../Service/sigin-external.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

@Component({
    moduleId: module.id,
    selector: 'sigin-external',
    templateUrl: '../View/sigin-external.html',
    providers: [SiginExternalService]
})

export class SiginExternalComponent implements OnInit {
    mensagemRetorno: string

    constructor(private siginExternalService: SiginExternalService,
        private route: ActivatedRoute,
        private location: Location
    ) {

    }

    ngOnInit(): void {

         var hash = window.location.hash.substr(1);

          var result: any = hash.split('&').reduce(function (result : any, item: string) {
                var parts = item.split('=');
                result[parts[0]] = parts[1];
                return result;
            }, {});
        this.siginExternalService.getToken(result.access_token);
    }

}