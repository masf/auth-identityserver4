import 'rxjs/add/operator/map'
import 'rxjs/Rx';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Login } from '../Model/login';

@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: '../View/login.html'
})

export class LoginComponent implements OnInit {
    mensagemRetorno: string
    modelo : Login

    ngOnInit(): void {
        this.modelo = new Login();
    }

}